//
//  DogEar.m
//  dogear
//
//  Created by Joy Tao on 12/3/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "DogEar.h"
#import "Cat.h"

@implementation DogEar

@dynamic title;
@dynamic note;
@dynamic created;
@dynamic updated;
@dynamic hasReminder;
@dynamic flagged;
@dynamic image;
@dynamic category;
@dynamic reminder;
@end
