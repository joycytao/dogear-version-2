//
//  DogEar.h
//  dogear
//
//  Created by Joy Tao on 12/3/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cat, Reminder;

@interface DogEar : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSDate * updated;
@property (nonatomic, retain) NSNumber * hasReminder;
@property (nonatomic, retain) NSNumber * flagged;
@property (nonatomic, retain) NSData * image;

@property (nonatomic, retain) Cat *category;
@property (nonatomic, retain) Reminder *reminder;

@end
