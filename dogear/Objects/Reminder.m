//
//  Reminder.m
//  dogear
//
//  Created by Joy Tao on 12/7/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "Reminder.h"
#import "DogEar.h"


@implementation Reminder

@dynamic frquency;
@dynamic date;
@dynamic dogear;

@end
