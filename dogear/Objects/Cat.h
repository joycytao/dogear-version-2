//
//  Category.h
//  dogear
//
//  Created by Joy Tao on 12/3/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Cat : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * piority;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSSet *dogears;
@end

@interface Cat (CoreDataGeneratedAccessors)

- (void)addDogearsObject:(NSManagedObject *)value;
- (void)removeDogearsObject:(NSManagedObject *)value;
- (void)addDogears:(NSSet *)values;
- (void)removeDogears:(NSSet *)values;

@end
