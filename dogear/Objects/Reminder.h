//
//  Reminder.h
//  dogear
//
//  Created by Joy Tao on 12/7/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DogEar;

@interface Reminder : NSManagedObject

@property (nonatomic, retain) NSNumber * frquency;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) DogEar *dogear;

@end
