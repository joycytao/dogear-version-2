//
//  AppDelegate.m
//  dogear
//
//  Created by Joy Tao on 12/3/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "AppDelegate.h"
#import "SharedRootViewController.h"
#import "MBProgressHUD.h"

@interface AppDelegate () <MBProgressHUDDelegate>
@property (nonatomic , retain) UIImageView * overlayView;
@end

@implementation AppDelegate

#pragma mark - 
- (void) showSplashView
{
    UIImage * bgImg ;
    CGRect windowBound = [[UIScreen mainScreen] bounds];
    if (windowBound.size.height >= 568.0f)
        bgImg = [UIImage imageNamed:@"dogear-bg-splash-new-568h"];
    else
        bgImg = [UIImage imageNamed:@"dogear-bg-splash-new"];
    self.overlayView = [[UIImageView alloc]initWithFrame:windowBound];
    self.overlayView.image = bgImg;
    
    [self.window addSubview:self.overlayView];
    
    MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.window animated:YES];
    hud.yOffset = 180.0f;
    [self.window addSubview:hud];
    
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    hud.delegate = self;
    
    // Show the HUD while the provided method executes in a new thread
    [hud showWhileExecuting:@selector(removeSplash) onTarget:self withObject:nil animated:YES];
//    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"HasLaunchedOnce"] == NO)
//    {
//        [[SharedAccountsManager sharedManager]requestFacebookPermission];
//        [[SharedAccountsManager sharedManager]requestTwitterPermission];
//    }
}

#pragma mark - UIApplication 

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    [[SharedHelper sharedHelper]setupAppearance];
    
    [self showSplashView];
    
    Class cls = NSClassFromString(@"UILocalNotification");
    if (cls)
    {
        UILocalNotification * notification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
        [[SharedHelper sharedHelper]showReminderDetailsFromNotifcation:notification];
        application.applicationIconBadgeNumber = 0;

    }
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kFacebookAccountActive];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kTwitterAccountActive];
    [[NSUserDefaults standardUserDefaults]synchronize];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
   
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification
{
    
    application.applicationIconBadgeNumber = 0;
}

#pragma mark- Private Method
- (void) removeSplash
{
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"HasLaunchedOnce"] == NO)
    {
        NSArray * array = [[SharedObjectManager sharedManager]getObjectsByEntityName:@"Cat" andPredict:nil andSortDescriptors:nil];
        if ([array count] == 0)
        {
            [[SharedHelper sharedHelper]addCategoryWithTitle:@"Newspapers" andPiority:0];
            [[SharedHelper sharedHelper]addCategoryWithTitle:@"Books" andPiority:1];
            [[SharedHelper sharedHelper]addCategoryWithTitle:@"Coupons" andPiority:2];
            [[SharedHelper sharedHelper]addCategoryWithTitle:@"Events" andPiority:3];
            [[SharedHelper sharedHelper]addCategoryWithTitle:@"Magazines" andPiority:4];
        }
    }
    else
        [[SharedHelper sharedHelper]recoverOldCategories];
    
    sleep(3.0f);
    
    [self.overlayView removeFromSuperview];
    self.rootViewController = [[SharedRootViewController alloc]init];
    self.window.rootViewController = self.rootViewController;
}




@end
