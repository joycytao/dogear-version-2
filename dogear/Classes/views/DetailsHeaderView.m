//
//  DetailsHeaderView.m
//  dogear
//
//  Created by Joy Tao on 12/4/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//
#define RADIUS 4.0f

#import "DetailsHeaderView.h"

@implementation DetailsHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        CGRect rect1 = CGRectMake(100.0f, 15.0f, 210.0f, 30.0f);
        CGRect tfFrame = UIEdgeInsetsInsetRect(rect1, UIEdgeInsetsMake(1.0f, 7.0f ,1.0f , 5.0f));
        
        UITextField * tf = [[UITextField alloc]initWithFrame:tfFrame];
        [tf setBackgroundColor:[UIColor clearColor]];
        [tf setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
        [tf setFont:[UIFont fontWithName:@"HelveticaNeue" size:14]];
        self.titleField = tf;
        
        CGRect rect2 = CGRectMake(100.0f, 45.0f, 210.0f, 60.0f);
        CGRect tvFrame = UIEdgeInsetsInsetRect(rect2, UIEdgeInsetsMake(1.0f, 1.0f , 1.0f , 5.0f));
        UITextView * tv = [[UITextView alloc]initWithFrame:tvFrame];
        [tv setBackgroundColor:[UIColor clearColor]];
        self.notesField = tv;
        
        CGRect rect3 = CGRectMake(10.0f, 15.0f, 90.0f, 90.0f);
        CGRect innerRect = UIEdgeInsetsInsetRect(rect3, UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f));
        self.thumbImage = [[UIImageView alloc]initWithFrame:innerRect];
        self.thumbImage.clipsToBounds = YES;
        self.thumbImage.contentMode = UIViewContentModeScaleAspectFill;
        
        
        [self addSubview:self.titleField];
        [self addSubview:self.notesField];
        [self addSubview:self.thumbImage];
        
    }
    
    return self;
}

#pragma mark - Draw Rect Private Functions

- (void) drawRect:(CGRect)rect
{
    [[UIColor lightGrayColor] setStroke];
    [[UIColor whiteColor] setFill];
    
    // Drawing ImageView Rect
    [self drawThumbViewWithRect:CGRectMake(10.0f, 15.0f, 90.0f, 90.0f)];
    [self drawTextFieldWithRect:CGRectMake(100.0f, 15.0f, 210.0f, 30.0f)];
    [self drawTextViewWithRect:CGRectMake(100.0f, 45.0f, 210.0f, 60.0f)];
    
    self.layer.masksToBounds = NO;
    self.layer.shadowColor = [[UIColor blackColor]CGColor];
    self.layer.shadowOffset = CGSizeMake(3.0f, 5.0f);
    self.layer.shadowOpacity = 0.7;
}

- (void) drawThumbViewWithRect:(CGRect)rect
{
    UIBezierPath * pathThumbImg = [UIBezierPath bezierPathWithRoundedRect:rect
                                                        byRoundingCorners:UIRectCornerBottomLeft|UIRectCornerTopLeft
                                                              cornerRadii:CGSizeMake(RADIUS, RADIUS)];
    [pathThumbImg setLineWidth:2.0f];
    [pathThumbImg stroke];
    [pathThumbImg fill];
}

- (void) drawTextFieldWithRect:(CGRect)rect
{
    UIBezierPath * pathThumbImg = [UIBezierPath bezierPathWithRoundedRect:rect
                                                        byRoundingCorners:UIRectCornerTopRight
                                                              cornerRadii:CGSizeMake(RADIUS, RADIUS)];
    [pathThumbImg setLineWidth:2.0f];
    [pathThumbImg stroke];
    [pathThumbImg fill];
    
}

- (void) drawTextViewWithRect:(CGRect)rect
{
    UIBezierPath * pathThumbImg = [UIBezierPath bezierPathWithRoundedRect:rect
                                                        byRoundingCorners:UIRectCornerBottomRight
                                                              cornerRadii:CGSizeMake(RADIUS, RADIUS)];
    [pathThumbImg setLineWidth:2.0f];
    [pathThumbImg stroke];
    [pathThumbImg fill];
    
}




@end
