//
//  DetailsHeaderView.h
//  dogear
//
//  Created by Joy Tao on 12/4/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface DetailsHeaderView : UIView

@property (nonatomic , strong) UIImageView * thumbImage;
@property (nonatomic , strong) UITextField * titleField;
@property (nonatomic , strong) UITextView * notesField;


@end
