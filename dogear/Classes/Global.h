//
//  Global.h
//  dogear
//
//  Created by Joy Tao on 12/3/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "DogEar.h"
#import "Cat.h"
#import "Reminder.h"

#import "SharedHelper.h"
#import "SharedCameraHelper.h"
#import "SharedAccountsManager.h"

#import "AppDelegate.h"
#import "SharedTableViewController.h"

#import "MBProgressHUD.h"

#import "NSString+Additions.h"
#import "UITextView+EULA.h"

#ifndef dogear_Global_h
#define dogear_Global_h

#define kAppDelegate ((AppDelegate*)[[UIApplication sharedApplication]delegate])

#define kDeviceOS [[[UIDevice currentDevice]systemVersion] floatValue]
#define kScreenBound [[UIScreen mainScreen]bounds]

#define iPadUI (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define iPhoneUI (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)

#define kPDFPageBounds CGRectMake(0, 0, 8.5 * 72, 11 * 72)

#define kDogEarBlue [UIColor colorWithRed:62.0f/255.0f green:153.0f/255.0f blue:166.0f/255.0f alpha:1.0]

#define kNotification @"dogear_notification"
#define kAppFacebookPermission @"app_facebook_permission"
#define kAppTwitterPermission @"app_twitter_permission"
#define kFacebookAccountActive @"app_facebook_active"
#define kTwitterAccountActive @"app_twitter_active"

#define kObjectCreatedDate @"dogear_insert_date"
#define kObjectTitle @"dogear_title"
#define kObjectCategory @"dogear_category"

#define kOldCategory @"BKCategory"
#define kOldDogEar @"BKDataCollections"


typedef enum {
    isFlaggedNone = 0,
    isFlaggedCasual = 1,
    isFlaggedSomewhatImportant = 2,
    isFlaggedImportant = 3,
    isFlaggedVeryImportant = 4,
    isFlaggedCrucial = 5,
}isFlaggedDogEar;

typedef enum {
    kModeNone = 0,
    kModeEditing = 1,
}EditingMode;


typedef enum {
    kModeByFlagged = 0,
    kModeByCategory = 1,
}ItemsViewingMode;

typedef enum {
    kAccountFacebook = 0,
    kAccountTwitter = 1,
}SocialNetworkingAccountType;

typedef enum {
    kAlertViewDeleteCategory = 100,
    kAlertViewAddCategory = 101,
    kAlertViewRetake = 102,
    kAlertViewShare = 103,
    kAlertViewSettings = 110,
}AlertViewTag;

typedef enum {
    kSwitchFacebook = 10,
    kSwitchTwitter = 11,
    kSwitchNotification = 20,
}SwitchTag;

#endif
