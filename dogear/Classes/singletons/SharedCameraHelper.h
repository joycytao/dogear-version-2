//
//  SharedCameraHelper.h
//  dogear
//
//  Created by Joy Tao on 12/7/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedCameraHelper : NSObject
+ (SharedCameraHelper*) sharedHelper;
- (UIImagePickerController*) activateImagePickerWithSource:(UIImagePickerControllerSourceType)sourceType inController:(id)controller;

@end
