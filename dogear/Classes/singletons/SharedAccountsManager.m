//
//  SharedAccountsManager.m
//  dogear
//
//  Created by Joy Tao on 12/20/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "SharedAccountsManager.h"

@implementation SharedAccountsManager

+(SharedAccountsManager*) sharedManager
{
    static SharedAccountsManager * sharedManager = nil;
    
    @synchronized(self){
        if (sharedManager == nil)
            sharedManager = [[super allocWithZone:NULL] init];
    }
    return sharedManager;
}

+(id) allocWithZone:(NSZone *)zone
{
    return [self sharedManager];
}

- (void) requestFacebookPermission
{
    ACAccountStore * accountStore = [[ACAccountStore alloc]init];
    ACAccountType * type = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    NSDictionary *options = @{ACFacebookAppIdKey : @"311343948974976",
                              ACFacebookPermissionsKey : @[@"email", @"publish_stream"],
                              ACFacebookAudienceKey:ACFacebookAudienceFriends};
    
    [accountStore requestAccessToAccountsWithType:type
                                          options:options
                                       completion:^(BOOL granted, NSError * error)
     {
         NSString * message;
         if (granted)
         {
             if (!error)
             {
                 [[NSUserDefaults standardUserDefaults] setBool:YES forKey: kAppFacebookPermission];
                 
                 [[NSUserDefaults standardUserDefaults]synchronize];
                 message = [NSString stringWithFormat:@"DogEar successfully connected to your Facebook account."];
             }
             else
             {
                 [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kAppFacebookPermission];
                 message = [NSString stringWithFormat:@"In order to connect, please set up Facebook account in Device Settings."];
             }
         }
         else
         {
             [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kAppFacebookPermission];
             message = [NSString stringWithFormat:@"If you have not opted to allow DogEar to post on your behalf, please login and post from the Facebook app."];
         }
         
         __block UIAlertView * av = [[UIAlertView alloc]initWithTitle:@"DogEar" message:message delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles: nil];
         dispatch_async(dispatch_get_main_queue(), ^(void)
                        {
            [[NSUserDefaults standardUserDefaults]synchronize];
             [av show];
         });
     }];

}

- (void) requestTwitterPermission
{
    ACAccountStore * accountStore = [[ACAccountStore alloc]init];
    ACAccountType * type = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    [accountStore requestAccessToAccountsWithType:type
                                          options:nil
                                       completion:^(BOOL granted, NSError * error)
     {
         NSString * message;
         if (granted)
         {
             if (!error)
             {
                 [[NSUserDefaults standardUserDefaults] setBool:YES forKey: kAppTwitterPermission];
                 
                 [[NSUserDefaults standardUserDefaults]synchronize];
                 message = [NSString stringWithFormat:@"DogEar successfully connected to your Twitter account."];
             }
             else
             {
                 [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kAppTwitterPermission];
                 [[NSUserDefaults standardUserDefaults]synchronize];
                 message = [NSString stringWithFormat:@"In order to connect, please set up Twitter account in Device Settings."];
             }
         }
         else
         {
             [[NSUserDefaults standardUserDefaults] setBool:NO forKey:kAppTwitterPermission];
             [[NSUserDefaults standardUserDefaults]synchronize];
             message = [NSString stringWithFormat:@"If you have not opted to allow DogEar to post on your behalf, please login and post from the Twitter app."];
         }
         
         __block UIAlertView * av = [[UIAlertView alloc]initWithTitle:@"DogEar" message:message delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles: nil];
         dispatch_async(dispatch_get_main_queue(), ^(void)
                        {
                            [av show];
                        });
         
     }];

}

@end
