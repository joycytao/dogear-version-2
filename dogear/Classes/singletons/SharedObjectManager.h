//
//  SharedObjectsManager.h
//  dogear
//
//  Created by Joy Tao on 12/3/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedObjectManager : NSObject
+ (SharedObjectManager*) sharedManager;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSManagedObjectContext *) objectContext;

- (void) saveContext;
- (NSArray*) getObjectsByEntityName:(NSString*)name andPredict:(NSPredicate*)predicate andSortDescriptors:(NSArray*)descriptors;
@end
