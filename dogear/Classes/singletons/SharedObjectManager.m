//
//  SharedObjectsManager.m
//  dogear
//
//  Created by Joy Tao on 12/3/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "SharedObjectManager.h"

@interface SharedObjectManager ()
@property (nonatomic , strong) NSManagedObjectContext * objectContext;
- (NSString *)sharedDocumentPath;
@end

@implementation SharedObjectManager
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

@synthesize objectContext = _objectContext;

#pragma mark - Singleton Syntax

+(SharedObjectManager*) sharedManager
{
    static SharedObjectManager * sharedManager = nil;
    
    @synchronized(self){
        if (sharedManager == nil)
            sharedManager = [[super allocWithZone:NULL] init];
    }
    return sharedManager;
}

+(id) allocWithZone:(NSZone *)zone
{
    return [self sharedManager];
}

#pragma mark - Core Data stack

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil) {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"DogEar" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    NSString * storePath = [[self sharedDocumentPath]stringByAppendingPathComponent:@"DogEar.sqlite"];
    NSURL * storeURL = [NSURL fileURLWithPath:storePath];
    
    NSError *error = nil;
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
    						 [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
    						 [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

- (NSString *)sharedDocumentPath;
{
    static NSString *sharedDocumentsPath = nil;
	if (sharedDocumentsPath)
		return sharedDocumentsPath;
    
	// Compose a path to the <Library>/Database directory
	NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	sharedDocumentsPath = [libraryPath stringByAppendingPathComponent:@"Database"];
    
	// Ensure the database directory exists
	NSFileManager *manager = [NSFileManager defaultManager];
	BOOL isDirectory;
	if (![manager fileExistsAtPath:sharedDocumentsPath isDirectory:&isDirectory] || !isDirectory) {
		NSError *error = nil;
		NSDictionary *attr = [NSDictionary dictionaryWithObject:NSFileProtectionComplete
                                                         forKey:NSFileProtectionKey];
		[manager createDirectoryAtPath:sharedDocumentsPath
		   withIntermediateDirectories:YES
                            attributes:attr
                                 error:&error];
		if (error)
			NSLog(@"Error creating directory path: %@", [error localizedDescription]);
	}
    
	return sharedDocumentsPath;
}
#pragma mark - Public Method
- (void) saveContext
{
    NSError * error = nil;
    [[self objectContext]save:&error];
     if (error != nil) NSLog(@"error: %@",[error localizedDescription]);
}

- (NSManagedObjectContext*) objectContext
{
    if (_objectContext != nil) {
        return _objectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _objectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_objectContext setPersistentStoreCoordinator:coordinator];
    }
    return _objectContext;
}

- (NSArray*) getObjectsByEntityName:(NSString *)name andPredict:(NSPredicate *)predicate andSortDescriptors:(NSArray *)descriptors
{
    NSFetchRequest * request = [[NSFetchRequest alloc]init];
    NSEntityDescription * entity = [NSEntityDescription entityForName:name inManagedObjectContext:[self objectContext]];
    [request setEntity:entity];
    [request setSortDescriptors:descriptors];
    
    if (predicate != nil)
        [request setPredicate:predicate];
    
    NSError * error = nil;
    NSArray * array = [[self objectContext] executeFetchRequest:request error:&error];
    return array;
}


@end
