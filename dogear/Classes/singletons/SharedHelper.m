//
//  DataHelper.m
//  dogear
//
//  Created by Joy Tao on 12/4/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "SharedHelper.h"
#import "SharedNavigationController.h"
#import "DogEarObject.h"

@implementation SharedHelper

+(SharedHelper*) sharedHelper
{
    static SharedHelper * sharedHelper = nil;
    
    @synchronized(self){
        if (sharedHelper == nil)
            sharedHelper = [[super allocWithZone:NULL] init];
    }
    return sharedHelper;
}

+(id) allocWithZone:(NSZone *)zone
{
    return [self sharedHelper];
}

- (void) setupAppearance
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:UIStatusBarAnimationNone];
    
    [[UISearchBar appearance]setScopeBarButtonTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor darkGrayColor], UITextAttributeTextColor, nil] forState:UIControlStateNormal];
    
    [[UISegmentedControl appearance]setTintColor:(kDeviceOS >= 7.0f)? [UIColor whiteColor]:[UIColor clearColor]];
    
    [[UIDatePicker appearance]setBackgroundColor:(kDeviceOS >= 7.0f)? [UIColor whiteColor]:[UIColor clearColor]];
    
    [[UIBarButtonItem appearance]setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys: [UIColor whiteColor], UITextAttributeTextColor,
                                                         [UIFont boldSystemFontOfSize:14.0f], UITextAttributeFont , nil] forState:UIControlStateNormal ];
    [[UINavigationBar appearance]setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys: [UIColor whiteColor], UITextAttributeTextColor,
      [UIFont boldSystemFontOfSize:18.0f] , UITextAttributeFont ,
      [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8],UITextAttributeTextShadowColor,
      [NSValue valueWithUIOffset:UIOffsetMake(0, 0)],
      UITextAttributeTextShadowOffset,nil]];
}

- (void) showReminderDetailsFromNotifcation:(UILocalNotification*)notice
{
    
}

- (void) recoverOldCategories
{
    NSArray * newCats = [[SharedObjectManager sharedManager]getObjectsByEntityName:@"Cat" andPredict:nil andSortDescriptors:nil];

    
    NSArray * categories = [[NSUserDefaults standardUserDefaults]objectForKey:kOldCategory];
    if ([newCats count] == 0 &&[categories count] > 0 )
    {
        for (int i = 0; i < [categories count] ; i++)
        {
            [self addCategoryWithTitle:[categories objectAtIndex:i] andPiority:i];
        }
    }
    [[SharedObjectManager sharedManager]saveContext];
}
- (void) recoverOldDogEarObjects
{
    NSArray * dogears = [[SharedObjectManager sharedManager]getObjectsByEntityName:@"DogEar" andPredict:nil andSortDescriptors:nil];
    if ([dogears count] > 0) return;
    
    NSArray * categories = [[SharedObjectManager sharedManager]getObjectsByEntityName:@"Cat" andPredict:nil andSortDescriptors:nil];
    
    for (int c = 0; c < [categories count]; c++)
    {
        NSData * data = [[[NSUserDefaults standardUserDefaults]objectForKey:kOldDogEar] objectForKey:[[categories objectAtIndex:c] title]];
        NSMutableArray * decodedCollections = [NSMutableArray arrayWithArray:[NSKeyedUnarchiver unarchiveObjectWithData: data]];
        
        for (DogEarObject * object in decodedCollections)
        {
            DogEar * newDogEar = [NSEntityDescription insertNewObjectForEntityForName:@"DogEar"inManagedObjectContext:[[SharedObjectManager sharedManager] objectContext]];
            newDogEar.title = object.title;
            newDogEar.note = object.note;
            newDogEar.flagged = [NSNumber numberWithInteger:[object.flagged integerValue] + 1];
            newDogEar.created = object.insertedDate;
            newDogEar.image = UIImagePNGRepresentation([UIImage imageWithContentsOfFile:object.imagePath]);
            newDogEar.category = (Cat*)[categories objectAtIndex:c];
            
            Reminder * newReminder = [NSEntityDescription insertNewObjectForEntityForName:@"Reminder"inManagedObjectContext:[[SharedObjectManager sharedManager] objectContext]];
            newReminder.frquency = object.repeatingReminder;
            newReminder.date = object.reminderDate;
            newDogEar.reminder = newReminder;
        }

        [[SharedObjectManager sharedManager]saveContext];

    }
}


- (NSArray *) flaggedTitles
{
    NSArray * flaggeds = [NSArray arrayWithObjects:@"Casual",@"Somewhat Important", @"Important", @"Very Important",@"Crucial", nil ];
    return flaggeds;
}

- (NSArray *) reminderFrquency
{
    NSArray * frqs = [NSArray arrayWithObjects:@"Never",@"Every Hour", @"Every Day", @"Every Week",@"Every Month", @"Every Year", nil ];
    return frqs;
}

- (NSData *) generatePDFDataForPrintingWithImage:(UIImage*)image
{
    NSMutableData *pdfData = [NSMutableData data];
    UIGraphicsBeginPDFContextToData(pdfData, kPDFPageBounds, nil);
    UIGraphicsBeginPDFPage();
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    //    [self drawStuffInContext:ctx];  // Method also usable from drawRect:.
    UIImageView * imageView = [[UIImageView alloc]initWithImage:image];
    [imageView.layer renderInContext:ctx];
    
    UIGraphicsEndPDFContext();
    return pdfData;
}


- (void) addCategoryWithTitle:(NSString*)title andPiority:(NSInteger)piority
{
    Cat * newCategory = [NSEntityDescription insertNewObjectForEntityForName:@"Cat"inManagedObjectContext:[[SharedObjectManager sharedManager] objectContext]];
    newCategory.title = title;
    newCategory.piority = [NSNumber numberWithInteger:piority];
    newCategory.created = [NSDate date];
    
    [[SharedObjectManager sharedManager]saveContext];
}

- (void) refreshCategoryFromArray:(NSArray*)array
{
    for (int order = 0; order < [array count]; order++)
    {
        Cat * this = [array objectAtIndex:order];
        this.piority = [NSNumber numberWithInteger:order];
    }
    [[SharedObjectManager sharedManager]saveContext];
}

- (void) addNotifcations
{
    NSArray * array = [[SharedObjectManager sharedManager]getObjectsByEntityName:@"DogEar" andPredict:[NSPredicate predicateWithFormat:@"hasReminder == %@",[NSNumber numberWithBool:YES]] andSortDescriptors:nil];
    for (DogEar * aDogear in array)
    {
        UILocalNotification * reminder = [[UILocalNotification alloc]init];
        if (reminder == nil) return;
        reminder.fireDate = aDogear.reminder.date;
        reminder.timeZone = [NSTimeZone defaultTimeZone];
        
        reminder.alertBody = [NSString stringWithFormat:@"%@: %@",aDogear.category.title, aDogear.title];
        reminder.hasAction = TRUE;
        reminder.alertAction = @"Check it";
        reminder.soundName = UILocalNotificationDefaultSoundName;
        reminder.applicationIconBadgeNumber = 1;
        if ([aDogear.reminder.frquency integerValue] == 0 ) reminder.repeatInterval = 0;
        else if ([aDogear.reminder.frquency integerValue] == 1) reminder.repeatInterval = NSHourCalendarUnit;
        else if ([aDogear.reminder.frquency integerValue] == 2) reminder.repeatInterval = NSDayCalendarUnit;
        else if ([aDogear.reminder.frquency integerValue] == 3) reminder.repeatInterval = NSWeekCalendarUnit;
        else if ([aDogear.reminder.frquency integerValue] == 4) reminder.repeatInterval = NSMonthCalendarUnit;
        else if ([aDogear.reminder.frquency integerValue] == 5) reminder.repeatInterval = NSYearCalendarUnit;
        
        NSDictionary * userDict = [NSDictionary dictionaryWithObjectsAndKeys:aDogear.category.title, kObjectCategory ,aDogear.title,kObjectTitle, aDogear.created, kObjectCreatedDate , nil];
        reminder.userInfo = userDict;
        [[UIApplication sharedApplication] scheduleLocalNotification:reminder];
    }
    
}
- (void) removeNotifications
{
    NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (UILocalNotification *notification in localNotifications)
    {
        [[UIApplication sharedApplication] cancelLocalNotification:notification];
    }
}

@end
