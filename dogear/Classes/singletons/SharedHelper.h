//
//  DataHelper.h
//  dogear
//
//  Created by Joy Tao on 12/4/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedHelper : NSObject
+ (SharedHelper*) sharedHelper;

- (void) setupAppearance;
- (void) showReminderDetailsFromNotifcation:(UILocalNotification*)notice;

- (void) recoverOldCategories;
- (void) recoverOldDogEarObjects;

- (NSArray *) flaggedTitles;
- (NSArray *) reminderFrquency;
- (NSData *) generatePDFDataForPrintingWithImage:(UIImage*)image;

- (void) addCategoryWithTitle:(NSString*)title andPiority:(NSInteger)piority;
- (void) refreshCategoryFromArray:(NSArray*)array;

- (void) addNotifcations;
- (void) removeNotifications;

@end
