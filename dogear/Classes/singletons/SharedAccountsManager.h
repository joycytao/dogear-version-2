//
//  SharedAccountsManager.h
//  dogear
//
//  Created by Joy Tao on 12/20/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>

@interface SharedAccountsManager : NSObject

+ (SharedAccountsManager*) sharedManager;

- (void) requestFacebookPermission;
- (void) requestTwitterPermission;

@end
