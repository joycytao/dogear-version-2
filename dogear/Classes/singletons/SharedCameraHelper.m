//
//  SharedCameraHelper.m
//  dogear
//
//  Created by Joy Tao on 12/7/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "SharedCameraHelper.h"

@implementation SharedCameraHelper

+(SharedCameraHelper*) sharedHelper
{
    static SharedCameraHelper * sharedHelper = nil;
    
    @synchronized(self){
        if (sharedHelper == nil)
            sharedHelper = [[super allocWithZone:NULL] init];
    }
    return sharedHelper;
}

+(id) allocWithZone:(NSZone *)zone
{
    return [self sharedHelper];
}

#pragma mark - Public Methods

- (UIImagePickerController*) activateImagePickerWithSource:(UIImagePickerControllerSourceType)sourceType inController:(id)controller
{
    UIImagePickerController * picker = [[UIImagePickerController alloc]init];
    picker.delegate = controller;
    picker.navigationController.delegate = controller;
    
    picker.sourceType = sourceType;
    picker.allowsEditing = YES;
    if (sourceType == UIImagePickerControllerSourceTypeCamera)
        picker.showsCameraControls = YES;
    
    return picker;
    
}
@end
