//
//  CategoryViewController.m
//  dogear
//
//  Created by Joy Tao on 12/3/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "MainTableViewController.h"
#import "DogEarsListViewController.h"
#import "FlaggedTableViewController.h"
#import "DogEarPhotoViewController.h"

@interface MainTableViewController () <UISearchDisplayDelegate>
@property (nonatomic , strong) NSArray * categories;

@property (nonatomic , strong) UISearchDisplayController * searchController;
@property (nonatomic , strong) NSMutableArray * searchedObjects;

@end

@implementation MainTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.categories = [[SharedObjectManager sharedManager]getObjectsByEntityName:@"Cat" andPredict:nil andSortDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"piority" ascending:YES],[NSSortDescriptor sortDescriptorWithKey:@"created" ascending:YES], nil]];

    [self customizeHeaderView];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Custom Header View

- (void) customizeHeaderView
{
    UISearchBar * searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.frame.size.width, 50.0f)];
    self.searchController = [[UISearchDisplayController alloc]initWithSearchBar:searchBar contentsController:self];
    self.searchController.searchResultsDataSource = self;
    self.searchController.searchResultsDelegate = self;
    self.searchController.delegate = self;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if ([tableView isEqual:self.searchController.searchResultsTableView])
        return 1;
    else
        return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.searchController.searchResultsTableView])
        return [self.searchedObjects count];
    else
    {
        if (section == 0)return 1;
        else return [self.categories count];
    }
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    if (cell == nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    if ([tableView isEqual:self.searchController.searchResultsTableView])
        cell.textLabel.text = [(DogEar*)[self.searchedObjects objectAtIndex:indexPath.row] title];
    else
    {
        if (indexPath.section == 0)
            cell.textLabel.text = @"Flagged DogEars";
        else
            cell.textLabel.text = [(Cat*)[self.categories objectAtIndex:indexPath.row] title];
    }
    
    return cell;
}

#pragma mark - UITableView Delegate

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) return 10.0f;
    else return 0.0f;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual:self.searchController.searchResultsTableView])
    {
        DogEar * selected = [self.searchedObjects objectAtIndex:indexPath.row];
        DogEarPhotoViewController * vc = [[DogEarPhotoViewController alloc]init];
        [vc setThisDogear:selected];
        
        [self.navigationController pushViewController:vc animated:YES];

    }
    else
    {
        if (indexPath.section == 0)
        {
            FlaggedTableViewController * vc = [[FlaggedTableViewController alloc]initWithStyle:UITableViewStyleGrouped andMode:kModeNone];
            vc.title = @"Flagged";
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            Cat * selected = [self.categories objectAtIndex:indexPath.row];
            
            DogEarsListViewController * vc = [[DogEarsListViewController alloc]initWithStyle:UITableViewStylePlain andMode:kModeByCategory];
            NSArray * array = [[SharedObjectManager sharedManager]getObjectsByEntityName:@"DogEar" andPredict:[NSPredicate predicateWithFormat:@"category.title == %@", selected.title] andSortDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"updated" ascending:NO], nil]];
            [vc setDogears:array];
            [vc.navigationItem setTitle:selected.title];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    
    
}


#pragma mark - UIsearchController Delegate Method

- (void) filterContentForSearchText:(NSString*)searchText andScope:(NSString*)scope
{
    if ([self.searchedObjects count] > 0)
        [self.searchedObjects removeAllObjects];
    
    NSArray * allDogs = [[SharedObjectManager sharedManager]getObjectsByEntityName:@"DogEar" andPredict:[NSPredicate predicateWithFormat:@"title contains[cd] %@ OR note contains [cd] %@" ,searchText, searchText] andSortDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"created" ascending:YES], nil]];
    
    self.searchedObjects = [NSMutableArray arrayWithArray:allDogs];
}

- (BOOL) searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                            andScope:[[self.searchController.searchBar scopeButtonTitles] objectAtIndex:[self.searchController.searchBar selectedScopeButtonIndex]]];
    return YES;
}

- (BOOL) searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    [self filterContentForSearchText:[controller.searchBar text]
                            andScope:[[self.searchController.searchBar scopeButtonTitles] objectAtIndex:[self.searchController.searchBar selectedScopeButtonIndex]]];
    return YES;
}

- (void) searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller
{
    if ([self.searchedObjects count]> 0)
        self.searchedObjects = nil;
    
    [self customizeHeaderView];
}


@end
