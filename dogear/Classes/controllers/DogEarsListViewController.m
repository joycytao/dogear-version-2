//
//  DogEarsListViewController.m
//  dogear
//
//  Created by Joy Tao on 12/7/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "DogEarsListViewController.h"
#import "DogEarPhotoViewController.h"

@interface DogEarsListViewController () <UIAlertViewDelegate>
@property (nonatomic) ItemsViewingMode thisMode;
@end

@implementation DogEarsListViewController
@synthesize dogears = _dogears;

- (void) setDogears:(NSArray *)thisArray
{
    if (_dogears == thisArray) return;
    _dogears = thisArray;
    self.tableView.separatorStyle = ([_dogears count] == 0) ?UITableViewCellSeparatorStyleNone : UITableViewCellSeparatorStyleSingleLine;
    
    [self.tableView reloadData];
}

- (id)initWithStyle:(UITableViewStyle)style andMode:(ItemsViewingMode)mode;
{
    self = [super initWithStyle:style];
    if (self) {
        self.thisMode = mode;
        NSLog(@"initWithStyle:%i",self.thisMode);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.thisMode == kModeByCategory)
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Rename" style:UIBarButtonItemStyleBordered target:self action:@selector(renameCategory)];
        
        CGRect frame = CGRectMake(0.0f, 0.0f, self.tableView.frame.size.width, 44.0f);
        UIView * headerview = [[UIView alloc]initWithFrame:frame];
        headerview.backgroundColor = [UIColor clearColor];
        
        UISegmentedControl * seg = [[UISegmentedControl alloc]initWithItems:[NSArray arrayWithObjects:@"Most Recent", @"Most Important", nil]];
        seg.frame = UIEdgeInsetsInsetRect(frame, UIEdgeInsetsMake(5.0f, 10.0f, 5.0f, 10.0f));
        seg.center = CGPointMake(self.view.center.x, seg.center.y);
        [seg addTarget:self action:@selector(didChangeSegmentControl:) forControlEvents:UIControlEventValueChanged];
        seg.segmentedControlStyle = UISegmentedControlStyleBar;
        seg.selectedSegmentIndex = 0;
        
        [headerview addSubview:seg];
        
        self.tableView.tableHeaderView = headerview;
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIBarButton Method
- (void) renameCategory
{
    UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"Add Category" message:@"Category Name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
    alertview.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alertview show];
}

#pragma mark - UISegmentControl Method

- (void)didChangeSegmentControl:(UISegmentedControl *)control
{
    switch (control.selectedSegmentIndex) {
        case 0:
        {
            self.dogears = [[SharedObjectManager sharedManager]getObjectsByEntityName:@"DogEar" andPredict:[NSPredicate predicateWithFormat:@"category.title == %@", self.navigationItem.title] andSortDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"updated" ascending:NO], nil]];
                self.tableView.separatorStyle = ([self.dogears count] == 0) ?UITableViewCellSeparatorStyleNone : UITableViewCellSeparatorStyleSingleLine;
            
            [self.tableView reloadData];
        }
            break;
        case 1:
        {
            self.dogears = [[SharedObjectManager sharedManager]getObjectsByEntityName:@"DogEar" andPredict:[NSPredicate predicateWithFormat:@"category.title == %@", self.navigationItem.title] andSortDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"flagged" ascending:NO], nil]];
            self.tableView.separatorStyle = ([self.dogears count] == 0) ?UITableViewCellSeparatorStyleNone : UITableViewCellSeparatorStyleSingleLine;
            [self.tableView reloadData];

        }
            break;
            
        default:
            break;
    }

}
#pragma mark - UIAlertView Delegate

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != alertView.cancelButtonIndex)
    {
        NSArray * categories = [[SharedObjectManager sharedManager]getObjectsByEntityName:@"Cat" andPredict:[NSPredicate predicateWithFormat:@"title == %@", self.navigationItem.title] andSortDescriptors:nil];
        [(Cat*)[categories lastObject] setTitle:[[alertView textFieldAtIndex:0] text]];

        [[SharedObjectManager sharedManager]saveContext];
        
        self.navigationItem.title = [[alertView textFieldAtIndex:0] text];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.dogears count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    cell.textLabel.text = [(DogEar*)[self.dogears objectAtIndex:indexPath.row] title];
    cell.detailTextLabel.text = [NSString shortDateAndTimeStyleStringFromDate:[(DogEar*)[self.dogears objectAtIndex:indexPath.row] updated]];
    cell.imageView.image = [UIImage imageWithData:[(DogEar*)[self.dogears objectAtIndex:indexPath.row] image]];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

#pragma mark - UITableview Delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DogEarPhotoViewController * vc = [[DogEarPhotoViewController alloc]init];
    [vc setThisDogear:[self.dogears objectAtIndex:indexPath.row]];
    [vc.navigationItem setTitle:[(DogEar*)[self.dogears objectAtIndex:indexPath.row] title]];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60.0f;
}

@end
