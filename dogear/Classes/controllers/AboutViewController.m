//
//  de_AboutViewController.m
//  DogEar
//
//  Created by Joy Tao on 2/1/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "AboutViewController.h"
#import "AboutHeaderView.h"

#import <MessageUI/MessageUI.h>
#import "UITextView+EULA.h"

@interface AboutViewController () <MFMailComposeViewControllerDelegate>

@end

@implementation AboutViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AboutHeaderView * aboutHeaderView = [[AboutHeaderView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 240.0f)];
    self.tableView.tableHeaderView = aboutHeaderView;
    self.title = @"About";
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.scrollEnabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    switch (indexPath.section) {
        case 0:
            cell.textLabel.text = @"Agreement";
            break;
        case 1:
            cell.textLabel.text = @"Email Support";
            break;
        default:
            break;
    }
    cell.backgroundColor = [UIColor colorWithRed:62.0f/255.0f green:153.0f/255.0f blue:166.0f/255.0f alpha:1.0];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0:
        {
            UIViewController * vc = [[UIViewController alloc]init];
            UITextView * eulaTextView = [[UITextView alloc]EndUserLicenseAgreenment];
            eulaTextView.frame = vc.view.bounds;

            eulaTextView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 109.0f, 0.0f);
            eulaTextView.scrollIndicatorInsets = UIEdgeInsetsMake(0.0f, 0.0f, 89.0f, 0.0f);
            [vc.view addSubview:eulaTextView];
            
            [self.navigationController pushViewController:vc animated:YES];
            vc.title = @"Agreement";
        }
            
            break;
        case 1:
        {
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController * mailComposer = [[MFMailComposeViewController alloc]init];
                mailComposer.mailComposeDelegate = self;
                
                [mailComposer setToRecipients:[NSArray arrayWithObjects:@"dogear-support@bloomfieldknoble.com", nil]];
                [mailComposer setSubject:@"DogEar Support Query from Apple Device"];
                
                [self presentViewController:mailComposer animated:YES completion:nil];
            }
            else
            {
                UIAlertView * av = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Can't send this email, please try agian." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles: nil];
                [av show];
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark -MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [controller dismissViewControllerAnimated:YES completion:nil];
}
@end
