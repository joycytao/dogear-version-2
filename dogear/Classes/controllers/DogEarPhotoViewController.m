//
//  DogEarPhotoViewController.m
//  dogear
//
//  Created by Joy Tao on 12/7/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "DogEarPhotoViewController.h"
#import "DetailsTableViewController.h"

#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <Social/Social.h>


@interface DogEarPhotoViewController () <UIActionSheetDelegate, UIAlertViewDelegate , UINavigationControllerDelegate , UIImagePickerControllerDelegate ,UIPrintInteractionControllerDelegate , MFMailComposeViewControllerDelegate>

@end

@implementation DogEarPhotoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!self.thisDogear)
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Retake" style:UIBarButtonItemStyleBordered target:self action:@selector(retake)];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Contiune" style:UIBarButtonItemStyleBordered target:self action:@selector(continueSaving)];
        
        [self.fullScreenImg setImage:[UIImage imageWithData:self.imageData]];
        [self.fullScreenImg setContentMode: UIViewContentModeScaleAspectFit];
    }
    else
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Details" style:UIBarButtonItemStyleBordered target:self action:@selector(checkDetails)];
        [self.fullScreenImg setImage:[UIImage imageWithData:self.thisDogear.image]];
        [self.fullScreenImg setContentMode: UIViewContentModeScaleAspectFit];
        
        [kAppDelegate.rootViewController.tabBar setHidden:YES];
        
        UIBarButtonItem * sharedtem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"dogear-icon-share"] style:UIBarButtonItemStylePlain target:self action:@selector(sharedThis)];
        UIBarButtonItem * space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem * trashtem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteThis)];
        
        UIToolbar * toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0.0f , self.view.frame.origin.y + self.view.frame.size.height - 44.0f, self.view.frame.size.width, 44.0f)];
        [toolBar setItems:[NSArray arrayWithObjects:sharedtem,space,trashtem, nil]];
        [self.view addSubview:toolBar];
    }
}

- (void) viewDidDisappear:(BOOL)animated
{
    [kAppDelegate.rootViewController.tabBar setHidden:NO];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UINavigationItems

- (void) retake
{
    UIActionSheet * as = [[UIActionSheet alloc]initWithTitle:@"Retake" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Take Picture" otherButtonTitles:@"Choose from Library", nil];
    as.tag = kAlertViewRetake;
    [as showInView:self.view];
}

- (void) continueSaving
{
    DogEar * newDogEar = [NSEntityDescription insertNewObjectForEntityForName:@"DogEar"inManagedObjectContext:[[SharedObjectManager sharedManager] objectContext]];
    newDogEar.image = UIImagePNGRepresentation(self.fullScreenImg.image);
    
    DetailsTableViewController * vc = [[DetailsTableViewController alloc]initWithStyle:UITableViewStyleGrouped];
    [vc setADogear:newDogEar];
    [self.navigationController pushViewController:vc animated:NO];
    
}

- (void) checkDetails
{
    DetailsTableViewController * vc = [[DetailsTableViewController alloc]initWithStyle:UITableViewStyleGrouped];
    [vc setADogear:self.thisDogear];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) deleteThis
{
    if (self.thisDogear)
    {
        [[[SharedObjectManager sharedManager]objectContext]deleteObject:self.thisDogear];
        [[SharedObjectManager sharedManager]saveContext];
        
        [kAppDelegate.rootViewController setSelectedIndex:0];
    }
}

- (void) sharedThis
{
    UIActionSheet * as = [[UIActionSheet alloc]initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Share on Facebook",@"Share on Twitter",@"Email DogEar",@"Save To Camera Roll",@"Print", nil];
    as.tag = kAlertViewShare;
    [as showInView:self.view];
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex) return;
    [kAppDelegate.rootViewController setSelectedIndex:2];
}

#pragma mark - ActionSheet Delegate

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == kAlertViewRetake)
    {
        UIImagePickerController * picker;
        if (buttonIndex == actionSheet.destructiveButtonIndex)
            picker = [[SharedCameraHelper sharedHelper] activateImagePickerWithSource:UIImagePickerControllerSourceTypeCamera inController:self];
        else if (buttonIndex == 1)
            picker = [[SharedCameraHelper sharedHelper] activateImagePickerWithSource:UIImagePickerControllerSourceTypePhotoLibrary inController:self];
        [self presentViewController:picker animated:NO completion:nil];
    }
    else
    {
        if (buttonIndex == actionSheet.cancelButtonIndex) return;
        else
        {
            if (buttonIndex == 0)
            {
                if ([[NSUserDefaults standardUserDefaults]boolForKey:kFacebookAccountActive] == NO)
                {
                    UIAlertView * av = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please turn on Facebook Sharing option. " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Settings", nil];
                    [av show];
                    
                    return;
                }
                
                if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
                {
                    SLComposeViewController *fbSheet = [SLComposeViewController
                                                           composeViewControllerForServiceType:SLServiceTypeFacebook];
                    [fbSheet setInitialText:self.thisDogear.title];
                    [fbSheet addImage:[UIImage imageWithData:self.thisDogear.image]];
                    [self presentViewController:fbSheet animated:YES completion:nil];
                }
            }
            else if (buttonIndex == 1)
            {
                if ([[NSUserDefaults standardUserDefaults]boolForKey:kTwitterAccountActive] == NO)
                {
                    UIAlertView * av = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please turn on Facebook Sharing option. " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Settings", nil];
                    [av show];
                    
                    return;
                }
                
                if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
                {
                    SLComposeViewController *tweetSheet = [SLComposeViewController
                                                           composeViewControllerForServiceType:SLServiceTypeTwitter];
                    [tweetSheet setInitialText:self.thisDogear.title];
                    [tweetSheet addImage:[UIImage imageWithData:self.thisDogear.image]];
                    [self presentViewController:tweetSheet animated:YES completion:nil];
                }
            }
            else if (buttonIndex == 2)
            {
                if ([MFMailComposeViewController canSendMail])
                {
                    MFMailComposeViewController * composer = [[MFMailComposeViewController alloc] init];
                    composer.delegate = self;
                    [composer setSubject:[NSString stringWithFormat:@"%@, insert Date:%@",self.thisDogear.title,[NSString shortDateAndTimeStyleStringFromDate:self.thisDogear.updated]]];
                    
                    [composer addAttachmentData:self.thisDogear.image mimeType:@"image/png" fileName:[NSString stringWithFormat:@"%@.png",self.thisDogear.title]];
                    [self.navigationController presentViewController:composer animated:YES completion:nil];
                }
                else
                {
                    UIAlertView * av = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Can't send this email, please try agian." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles: nil];
                    [av show];
                }
            }
            else if (buttonIndex == 3)
            {
                UIImageWriteToSavedPhotosAlbum([UIImage imageWithData:self.thisDogear.image], self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
            }
            else
            {
                UIPrintInteractionController * printController = [UIPrintInteractionController sharedPrintController];
                
                void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
                ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
                    if (!completed && error) NSLog(@"Print error: %@", error);
                };
                
                NSData *pdfData = [[SharedHelper sharedHelper] generatePDFDataForPrintingWithImage:[UIImage imageWithData:self.thisDogear.image]];
                printController.printingItem = pdfData;
                printController.delegate = self;
                [printController presentAnimated:YES completionHandler:completionHandler];
            }
        }
    }
}

#pragma mark - UIImagePickerController Delegate

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    DogEar * newDogEar = [NSEntityDescription insertNewObjectForEntityForName:@"DogEar"inManagedObjectContext:[[SharedObjectManager sharedManager] objectContext]];
    newDogEar.image = UIImagePNGRepresentation(chosenImage);
    
    [picker dismissViewControllerAnimated:YES completion:^{
        DetailsTableViewController * vc = [[DetailsTableViewController alloc]initWithStyle:UITableViewStyleGrouped];
        [vc setADogear:newDogEar];
        [self.navigationController pushViewController:vc animated:NO];
    }];
    [self.fullScreenImg setImage:chosenImage];
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    kAppDelegate.rootViewController.selectedIndex = 0;
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    NSString * message;
    if (error != NULL)
        message = @"DogEar not saved to Camera Roll. Please try again";
    else  // No errors
        message = @"DogEar has successfully saved this image to your camera roll.";
    UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"DogEar" message:message delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles: nil];
    [alertView show];

}

#pragma mark - MFMailComposeViewController Delegate Method

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued.");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved: you saved the email message in the drafts folder.");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send.");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: the email message was not saved or queued, possibly due to an error.");
            break;
        default:
            NSLog(@"Mail not sent.");
            break;
    }
    // Remove the mail view
    [controller dismissViewControllerAnimated:YES completion:nil];
}
@end
