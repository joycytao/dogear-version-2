//
//  SettingViewController.m
//  dogear
//
//  Created by Joy Tao on 12/3/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "SettingViewController.h"
#import "SharedViewController.h"
#import "AboutViewController.h"

#import "MBProgressHUD.h"

@interface SettingViewController () <MBProgressHUDDelegate>

@end

@implementation SettingViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- UISwitch 

- (void) switchContol:(id)sender
{
    UISwitch * sw = (UISwitch*)sender;
    if (sw.tag == kSwitchNotification)
    {
        if (sw.on == YES)
            [[SharedHelper sharedHelper]addNotifcations];
        else
            [[SharedHelper sharedHelper]removeNotifications];
         [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:2], nil] withRowAnimation:UITableViewRowAnimationFade];

    }
    else if (sw.tag == kSwitchFacebook)
    {
        if (sw.on == YES)
        {
            if ([[NSUserDefaults standardUserDefaults]boolForKey:kAppFacebookPermission] == NO)
            {
                MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.dimBackground = YES;
                hud.labelText = @"Connecting...";
                hud.delegate = self;
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^ {
                    [[SharedAccountsManager sharedManager]requestFacebookPermission];
                    
                    dispatch_async(dispatch_get_main_queue(), ^(void)
                                   {
                                       [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                       [self performSelector:@selector(checkAccountsActive:) withObject:nil afterDelay:2.0f];
                                   });
                });

            }
            else
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kFacebookAccountActive];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kFacebookAccountActive];
            UIAlertView * alertview =  [[UIAlertView alloc]initWithTitle:@"Facebook" message:@"You have disconnected Facebook account. To share Dogear via Facebook, you have to re-connect to Facebook." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alertview show];
        }
        
    }
    else if (sw.tag == kSwitchTwitter)
    {
        if (sw.on == YES)
        {
            if ([[NSUserDefaults standardUserDefaults]boolForKey:kAppTwitterPermission] == NO)
            {
                MBProgressHUD * hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.dimBackground = YES;
                hud.labelText = @"Connecting...";
                hud.delegate = self;
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^ {
                    [[SharedAccountsManager sharedManager]requestTwitterPermission];
                    dispatch_async(dispatch_get_main_queue(), ^(void)
                                   {
                                    
                                       [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                                       [self performSelector:@selector(checkAccountsActive:) withObject:nil afterDelay:2.0f];
                                   });
                    
                });
            }
            else
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kTwitterAccountActive];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kTwitterAccountActive];
            UIAlertView * alertview =  [[UIAlertView alloc]initWithTitle:@"Twitter" message:@"You have disconnected Twitter account. To share Dogear via Twitter, you have to re-connect to Twitter." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alertview show];
        }
        
    }
    
    [[NSUserDefaults standardUserDefaults]synchronize];
}

#pragma mark - 
- (void) checkAccountsActive:(id)sender
{
    UISwitch * sw = (UISwitch*)sender;
    if (sw.tag == kSwitchTwitter)
    {
        if ([[NSUserDefaults standardUserDefaults]boolForKey:kAppTwitterPermission] == YES)
        {
            [sw setOn:YES animated:YES];
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kTwitterAccountActive];
        }
        
        else
        {
            [sw setOn:NO animated:YES];
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kTwitterAccountActive];
        }
        
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:1 inSection:1], nil] withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        if ([[NSUserDefaults standardUserDefaults]boolForKey:kAppFacebookPermission] == YES)
        {
            [sw setOn:YES animated:YES];
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:kFacebookAccountActive];
        }
        else
        {
            [sw setOn:NO animated:YES];
            [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kFacebookAccountActive];
        }
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:1], nil] withRowAnimation:UITableViewRowAnimationFade];
    }
}


#pragma mark-

- (void) getPermissionWithAccountType:(SocialNetworkingAccountType)accountType
{
    
    [self.tableView reloadData];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) return 2; //About, How To use
    else if (section == 1) return 2;
    else return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
   
    UISwitch * switchControl = [[UISwitch alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 80.0f, 44.0f)];
    [switchControl addTarget:self action:@selector(switchContol:) forControlEvents:UIControlEventValueChanged];
    cell.accessoryView = (indexPath.section != 0) ?  switchControl :nil;
    cell.accessoryType = (indexPath.section == 0) ? UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone;
    cell.selectionStyle = (indexPath.section == 0) ? UITableViewCellSelectionStyleBlue : UITableViewCellSelectionStyleNone;
    
    if (indexPath.section == 0)
        cell.textLabel.text =  (indexPath.row == 0) ? @"About" : @"How To Use";
    else if (indexPath.section == 1)
    {
        cell.textLabel.text = (indexPath.row == 0) ? @"Facebook" : @"Twitter";
        switchControl.on = (indexPath.row == 0) ? [[NSUserDefaults standardUserDefaults]boolForKey:kFacebookAccountActive] : [[NSUserDefaults standardUserDefaults]boolForKey:kTwitterAccountActive];
        switchControl.tag = 10 * indexPath.section + indexPath.row;

    }
    else
    {
        cell.textLabel.text = @"Notification";
        switchControl.on = [[NSUserDefaults standardUserDefaults]boolForKey:kNotification] ;
        switchControl.tag = 10 * indexPath.section + indexPath.row;

    }
    
    return cell;
}
#pragma mark - UITableView Delegate

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            AboutViewController * vc = [[AboutViewController alloc]initWithStyle:UITableViewStyleGrouped];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else
        {
            SharedViewController * vc = [[SharedViewController alloc]init];
            vc.isTutorial = YES;
            vc.navigationItem.title = @"How To Use";
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}

@end
