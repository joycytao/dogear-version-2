//
//  de_ReminderViewController.m
//  DogEar
//
//  Created by Joy Tao on 11/28/12.
//  Copyright (c) 2012 Joy Tao. All rights reserved.
//

#import "ReminderViewController.h"


@interface ReminderViewController ()
{
    UIDatePicker * picker;
    BOOL isExpandedTable;
    
}
@property (nonatomic , strong) Reminder * reminder;
@end

@implementation ReminderViewController

@synthesize thisDogEar = _thisDogEar;

- (void) setThisDogEar:(DogEar *)aDogEar
{
    if (_thisDogEar == aDogEar) return;
    _thisDogEar = aDogEar;
    
    isExpandedTable = [_thisDogEar.hasReminder boolValue];
    [self.tableView reloadData];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        self.tableView.scrollEnabled = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGRect bounds = [[UIScreen mainScreen]bounds];
    picker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0.0f, bounds.size.height - 250.0f -44.0f, bounds.size.width, 250.0f +44.0f)];
    picker.datePickerMode = UIDatePickerModeDateAndTime;
    
    NSDate * oneHourAheadDate = [[NSDate date] dateByAddingTimeInterval:60 * 60];
    picker.date = oneHourAheadDate;
    [picker addTarget:self
               action:@selector(changeDateReminder:)
     forControlEvents:UIControlEventValueChanged];
    [picker setBackgroundColor:[UIColor clearColor]];
    picker.hidden = YES;
    [self.view addSubview:picker];
        
    UIBarButtonItem * doneItem = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(addReminder)];
    self.navigationItem.rightBarButtonItem = doneItem;
    
    UIBarButtonItem * cancelItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAddingReminder)];
    self.navigationItem.leftBarButtonItem = cancelItem;
    self.navigationItem.title = @"Set A Reminder";
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UISwitch Method

- (void) switchTurnOnOff:(id)sender
{
    UISwitch * toggle = (UISwitch*)sender;
    
    if (toggle.on)
    {
        isExpandedTable = YES;
        [toggle setOn:NO animated:YES];
        
         self.reminder = [NSEntityDescription insertNewObjectForEntityForName:@"Reminder"inManagedObjectContext:[[SharedObjectManager sharedManager] objectContext]];
        self.reminder.date = [[NSDate date] dateByAddingTimeInterval:60 * 60];
    }
    else
    {
        isExpandedTable = NO;
        [toggle setOn:YES animated:YES];
        picker.hidden = YES;

    }
    
    [self.tableView reloadData];
}

- (void)changeDateReminder:(id)sender
{
    self.reminder.date = picker.date;
    [self.tableView reloadData];
}

- (void) addReminder
{
    self.thisDogEar.reminder = self.reminder;
    self.thisDogEar.hasReminder = [NSNumber numberWithBool:YES];
    [[SharedObjectManager sharedManager]saveContext];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void) cancelAddingReminder
{
    self.thisDogEar.hasReminder = [NSNumber numberWithBool:NO];
    [[[SharedObjectManager sharedManager]objectContext]deleteObject:self.reminder];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isExpandedTable == NO) return 1;
    else return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    if (indexPath.row == 0)
    {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.text = @"Remind Me On A Day";
    
        UISwitch * reminderSwitch = [[UISwitch alloc]initWithFrame:CGRectMake(0.0f, 0.0f, 80.0f, 44.0f)];
        [reminderSwitch addTarget:self action:@selector(switchTurnOnOff:) forControlEvents:UIControlEventValueChanged];
        reminderSwitch.on = isExpandedTable;
        cell.accessoryView = reminderSwitch;
    }
    
    else if (indexPath.row == 1)
            cell.textLabel.text = [NSString shortDateAndTimeStyleStringFromDate:self.reminder.date];
    else if (indexPath.row == 2)
    {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.text = @"Repeat";
        cell.detailTextLabel.text = [[[SharedHelper sharedHelper]reminderFrquency] objectAtIndex:[self.reminder.frquency integerValue]];
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 1:
        {
            picker.hidden = NO;
        }
            break;
        case 2:
        {
            

        }
            
            break;
        default:
            break;
    }
    
}

@end
