//
//  SharedRootViewController.m
//  dogear
//
//  Created by Joy Tao on 12/3/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "SharedRootViewController.h"
#import "SharedNavigationController.h"
#import "MainTableViewController.h"
#import "SettingViewController.h"
#import "SharedViewController.h"

@interface SharedRootViewController () <UIActionSheetDelegate , UITabBarControllerDelegate>
@end

@implementation SharedRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.delegate = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    MainTableViewController * vc1 = [[MainTableViewController alloc]initWithStyle:UITableViewStyleGrouped];
    SharedNavigationController * nc1 = [[SharedNavigationController alloc]initWithRootViewController:vc1];
    vc1.title = @"Browse";
    UITabBarItem * browseBtn = [[UITabBarItem alloc]initWithTitle:@"Browse" image:[UIImage imageNamed:@"dogear-icon-browse"] tag:0];
    [nc1 setTabBarItem:browseBtn];
    
    SharedViewController * vcCamera = [[SharedViewController alloc]init];
    SharedNavigationController * nc2 = [[SharedNavigationController alloc]initWithRootViewController:vcCamera];
    vcCamera.title = @"Camera";
    vcCamera.isTutorial = NO;
    UITabBarItem * DECameraBtn = [[UITabBarItem alloc]initWithTitle:@"Camera" image:[UIImage imageNamed:@"dogear-icon-camera"] tag:1];
    [nc2 setTabBarItem:DECameraBtn];
    
    
    SettingViewController * vc3 = [[SettingViewController alloc]initWithStyle:UITableViewStyleGrouped];
    SharedNavigationController * nc3 = [[SharedNavigationController alloc]initWithRootViewController:vc3];
    vc3.title = @"Settings";
    UITabBarItem * settingsBtn = [[UITabBarItem alloc]initWithTitle:@"Settings" image:[UIImage imageNamed:@"dogear-icon-settings"] tag:2];
    [nc3 setTabBarItem:settingsBtn];
	
    self.viewControllers = [NSArray arrayWithObjects: nc1 , nc2, nc3, nil];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"HasLaunchedOnce"] == NO)
    {
        [self setSelectedIndex:1];
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kNotification];
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kAppFacebookPermission];
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:kAppTwitterPermission];

        [[NSUserDefaults standardUserDefaults]synchronize];

    }
    else
    {
        [self setSelectedIndex:0];
        [[SharedHelper sharedHelper]recoverOldDogEarObjects];

    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITabBarController Delegate

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if (tabBarController.selectedIndex == 1)
    {
        SharedNavigationController * nc = [self.viewControllers objectAtIndex:1];
        [nc popToRootViewControllerAnimated:YES];

        UIActionSheet * as = [[UIActionSheet alloc]initWithTitle:@"Add New DogEar" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Take Picture" otherButtonTitles:@"Choose from Library", nil];
        [as showInView:viewController.view];
    }
}

#pragma mark - ActionSheet Delegate 

- (void) actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    SharedNavigationController * nc = [self.viewControllers objectAtIndex:1];
    SharedViewController * vc = [nc.viewControllers objectAtIndex:0];
    
    UIImagePickerController * picker;
    if (buttonIndex == actionSheet.destructiveButtonIndex)
        picker = [[SharedCameraHelper sharedHelper] activateImagePickerWithSource:UIImagePickerControllerSourceTypeCamera inController:vc];
    else if (buttonIndex == 1)
         picker = [[SharedCameraHelper sharedHelper] activateImagePickerWithSource:UIImagePickerControllerSourceTypePhotoLibrary inController:vc];
    else
        self.selectedIndex = 0;
    
    if (picker != nil)
        [self presentViewController:picker animated:YES completion:nil];
}

@end
