//
//  de_RepeatingLNViewController.m
//  DogEar
//
//  Created by Joy Tao on 11/29/12.
//  Copyright (c) 2012 Joy Tao. All rights reserved.
//

#import "ReminderFrequencyListController.h"
#import "ReminderViewController.h"

@interface ReminderFrequencyListController ()

@end

@implementation ReminderFrequencyListController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Set Repeat";
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[SharedHelper sharedHelper]reminderFrquency] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        if (cell == nil) cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    // Configure the cell...
    if([self.thisReminder.frquency integerValue] == indexPath.row)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    cell.textLabel.text = [[[SharedHelper sharedHelper]reminderFrquency] objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     UITableViewCell* cell =[tableView  cellForRowAtIndexPath:indexPath];
    if ([self.thisReminder.frquency integerValue] == indexPath.row)
        cell.accessoryType = UITableViewCellAccessoryNone;
    else
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
