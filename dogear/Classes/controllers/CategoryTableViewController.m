//
//  CategoryTableViewController.m
//  dogear
//
//  Created by Joy Tao on 12/4/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "CategoryTableViewController.h"

@interface CategoryTableViewController () <UIAlertViewDelegate>
{
    NSIndexPath * selectedPath;
    NSIndexPath * checkedPath;
}
@property (nonatomic , strong) NSArray * categories;
@property (nonatomic) EditingMode editingMode;
@end

@implementation CategoryTableViewController

- (id)initWithStyle:(UITableViewStyle)style andMode:(EditingMode)mode;
{
    self = [super initWithStyle:style];
    if (self) {
        self.editingMode = mode;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Category";
    
    self.categories = [[SharedObjectManager sharedManager]getObjectsByEntityName:@"Cat" andPredict:nil andSortDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"piority" ascending:YES],[NSSortDescriptor sortDescriptorWithKey:@"created" ascending:YES], nil]];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(editTable:) ];
    [self.tableView reloadData];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Editable Table Method

- (void) editTable:(id)sender{
	if(self.editing)
	{
		[super setEditing:NO animated:NO];
		[self.tableView setEditing:NO animated:NO];
        
		[self.navigationItem.rightBarButtonItem setTitle:@"Edit"];
		[self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStylePlain];
        
        self.navigationItem.leftBarButtonItem = nil;
        
        [[SharedObjectManager sharedManager]saveContext];
        
        self.categories = [[SharedObjectManager sharedManager]getObjectsByEntityName:@"Cat" andPredict:nil andSortDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"piority" ascending:YES],[NSSortDescriptor sortDescriptorWithKey:@"created" ascending:YES], nil]];
	}
	else
	{
		[super setEditing:YES animated:YES];
        
		[self.navigationItem.rightBarButtonItem setTitle:@"Done"];
		[self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
        
        
	}
    [self.tableView reloadData];

}
//
//- (void ) cancelEditing
//{
//    self.categories = [[SharedObjectManager sharedManager]getObjectsByEntityName:@"Cat" andPredict:nil andSortDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"piority" ascending:YES], nil]];
//    [self.tableView reloadData];
//}

#pragma mark - UIAlertView Delegate

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == kAlertViewDeleteCategory)
    {
        if (buttonIndex != alertView.cancelButtonIndex)
        {
            [self.tableView deleteRowsAtIndexPaths:@[selectedPath] withRowAnimation:UITableViewRowAnimationFade];
            Cat * deletedObj = [self.categories objectAtIndex:selectedPath.row];
            [[[SharedObjectManager sharedManager]objectContext]deleteObject:deletedObj];
            
        }
        selectedPath = nil;
    }
    else if (alertView.tag == kAlertViewAddCategory)
    {
        if (buttonIndex != alertView.cancelButtonIndex)
        {
            [self.tableView insertRowsAtIndexPaths:@[selectedPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            NSString * categoryName = [[alertView textFieldAtIndex:0] text];
            [[SharedHelper sharedHelper]addCategoryWithTitle:categoryName andPiority:[self.categories count]];
        }
        selectedPath = nil;
            
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSInteger count = [self.categories count];
    if (self.editing) count ++;
    return count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    if ((self.editing == YES) && (indexPath.row == [self.categories count]) )
        cell.textLabel.text = @"Add New Category";
    else
        cell.textLabel.text = [(Cat*)[self.categories objectAtIndex:indexPath.row] title];
    cell.selectionStyle = (self.editing) ?  UITableViewCellSelectionStyleNone : UITableViewCellSelectionStyleBlue;
    if (!self.editing)
    {
        if (self.thisDogear.category != nil )
        {
            NSInteger checkedRow = [self.categories indexOfObject:self.thisDogear.category];
            cell.accessoryType = (indexPath.row == checkedRow)? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
        }
    }
    return cell;
}

#pragma mark - UITableView Delegate 
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.editing) return;
    else
    {
        UITableViewCell * selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        if (self.thisDogear.category != nil)
        {
             NSInteger checkedRow = [self.categories indexOfObject:self.thisDogear.category];
            UITableViewCell * checkedCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:checkedRow inSection:0]];
            if (indexPath.row != checkedRow)
            {
                checkedCell.accessoryType = UITableViewCellAccessoryNone;
                selectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
                self.thisDogear.category = [self.categories objectAtIndex:indexPath.row];
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                checkedCell.accessoryType = UITableViewCellAccessoryNone;
                self.thisDogear.category = nil;
            }
        }
        else
        {
            selectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
            self.thisDogear.category = [self.categories objectAtIndex:indexPath.row];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.editing == YES) return YES;
    else return NO;
}
- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.editing == NO)
        return UITableViewCellEditingStyleNone;
    else if (indexPath.row == [self.categories count])
        return UITableViewCellEditingStyleInsert;
    else
        return UITableViewCellEditingStyleDelete;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        selectedPath = indexPath;
        // Delete the row from the data source
        
        UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"Delete Category" message:@"All photos will be deleted within this category. Are you sure you want to delete this category? " delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
        alertview.tag = kAlertViewDeleteCategory;
        [alertview show];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        selectedPath = indexPath;

        UIAlertView * alertview = [[UIAlertView alloc]initWithTitle:@"Add Category" message:@"Category Name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
        alertview.alertViewStyle = UIAlertViewStylePlainTextInput;
        alertview.tag = kAlertViewAddCategory;
        [alertview show];
    }   
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSMutableArray * new = [[NSMutableArray alloc]initWithArray:self.categories];
    Cat * fromObj = [self.categories objectAtIndex:fromIndexPath.row];

    [new removeObject:fromObj];
    [new insertObject:fromObj atIndex:toIndexPath.row];
    
    [[SharedHelper sharedHelper]refreshCategoryFromArray:new];
   
}


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (self.editing == YES) ? YES: NO;
}


@end
