//
//  DetailsViewController.m
//  dogear
//
//  Created by Joy Tao on 12/4/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "DetailsTableViewController.h"
#import "DetailsHeaderView.h"
#import "CategoryTableViewController.h"
#import "FlaggedTableViewController.h"
#import "ReminderViewController.h"

@interface DetailsTableViewController () <UITextViewDelegate , UITextFieldDelegate , UIPrintInteractionControllerDelegate>
{
    BOOL enableEditing;
}
@property (nonatomic , strong) DetailsHeaderView * headerView ;
@end

@implementation DetailsTableViewController
@synthesize aDogear = _aDogear;

- (void) setADogear:(DogEar *)theDogear
{
    if (_aDogear == theDogear) return;
        _aDogear = theDogear;
    [self.tableView reloadData];
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (self.aDogear.created == nil)
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Add" style:UIBarButtonItemStyleBordered target:self action:@selector(addDogEar)];
        self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancel)];
       
        enableEditing = YES;
        
    }
    else
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(edit:)];
        self.title = self.aDogear.title;
        enableEditing = NO;

    }
    
    [self createCustomHeader];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- Custom Header
- (void) createCustomHeader
{
    DetailsHeaderView * headerView = [[DetailsHeaderView alloc]initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 120.0f)];
    headerView.titleField.text = (self.aDogear.title == nil) ? @"":self.aDogear.title;
    headerView.titleField.placeholder = (self.aDogear.title == nil) ? @"Title":@"";
    headerView.titleField.delegate = self;
    headerView.titleField.enabled = enableEditing;
    
    
    headerView.notesField.text = (self.aDogear.note == nil) ? @"Note" : self.aDogear.note;
    headerView.notesField.delegate = self;
    [headerView.notesField setEditable:enableEditing];
    
    headerView.thumbImage.image = [UIImage imageWithData:self.aDogear.image];
    
    self.tableView.tableHeaderView = headerView;
}

#pragma mark - UIBarButtonItem Public Method

- (void) cancel
{
    [[[SharedObjectManager sharedManager]objectContext]deleteObject:self.aDogear];
    [[SharedObjectManager sharedManager]saveContext];
    
    [self.navigationController popViewControllerAnimated:NO];
    
    
}

- (void) edit:(id)sender
{
    if (enableEditing == NO)
    {
        self.navigationItem.rightBarButtonItem.title = @"Update";
        self.navigationItem.leftBarButtonItem =[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelEditing)];
        
        enableEditing = YES;
    }
    else
    {
        self.aDogear.updated = [NSDate date];
        [[SharedObjectManager sharedManager] saveContext];
        
        self.navigationItem.rightBarButtonItem.title = @"Edit";
        self.navigationItem.leftBarButtonItem = nil;
        
        enableEditing = NO;
    }
    [self createCustomHeader];

}

- (void) cancelEditing
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Edit" style:UIBarButtonItemStyleBordered target:self action:@selector(edit:)];
    self.navigationItem.leftBarButtonItem = nil;
    
    enableEditing = NO;
}

- (void) addDogEar
{
    if (self.aDogear.title == nil || self.aDogear.category == nil)
    {
        UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"Warning" message:@"Please enter a Title and Category for your DogEar." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles: nil];
        [alertView show];
        return;
    }
    
    self.aDogear.created = [NSDate date];
    self.aDogear.updated = [NSDate date];

    [[SharedObjectManager sharedManager] saveContext];
    kAppDelegate.rootViewController.selectedIndex = 0;
}

#pragma mark - UITextFieldDelegate Method
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.aDogear.title = textField.text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


#pragma mark - UITextViewDelegate Method
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    return YES;
}

- (void) textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"Note"])
    {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.aDogear.note = textView.text;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    if (indexPath.row == 0)
    {
        cell.textLabel.text = @"Category";
        cell.detailTextLabel.text = (self.aDogear.category == nil) ? @"(Required)" : self.aDogear.category.title;
    }
    else if (indexPath.row == 1)
    {
        cell.textLabel.text = @"Reminder";
        cell.detailTextLabel.text = ([self.aDogear.hasReminder boolValue] == NO) ? @"" : [NSString shortDateAndTimeStyleStringFromDate:self.aDogear.reminder.date];
    }
    else if (indexPath.row == 2)
    {
        cell.textLabel.text = @"Flagged";
        NSArray * flagged = [[SharedHelper sharedHelper]flaggedTitles];
        cell.detailTextLabel.text = ([self.aDogear.flagged integerValue] != 0)?[flagged objectAtIndex:[self.aDogear.flagged integerValue] - 1]:@"";
    }
    else if (indexPath.row == 3)
        cell.textLabel.text = @"Print";
    cell.accessoryType = (indexPath.row != 3) ? UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone;

    return cell;
}

#pragma mark - Table view delegate Method

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0)
    {
        if (enableEditing == NO) return;

        CategoryTableViewController * vc = [[CategoryTableViewController alloc]initWithStyle:UITableViewStyleGrouped andMode:kModeEditing];
        [vc setThisDogear:self.aDogear];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (indexPath.row == 2)
    {
        if (enableEditing == NO) return;

        FlaggedTableViewController * vc = [[FlaggedTableViewController alloc]initWithStyle:UITableViewStyleGrouped andMode:kModeEditing];
        [vc setThisDogear:self.aDogear];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (indexPath.row == 1)
    {
        if (enableEditing == NO) return;
        ReminderViewController * vc = [[ReminderViewController alloc]initWithStyle:UITableViewStyleGrouped];
        [vc setThisDogEar:self.aDogear];
        [self.navigationController pushViewController:vc animated:YES];

    }
    
    else if (indexPath.row == 3)
    {
        UIPrintInteractionController *printController = [UIPrintInteractionController sharedPrintController];
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) =
        ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
            if (!completed && error) NSLog(@"Print error: %@", error);
            [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
        };
        
        NSData *pdfData = [[SharedHelper sharedHelper] generatePDFDataForPrintingWithImage:[UIImage imageWithData:self.aDogear.image]];
        printController.delegate = self;
        printController.printingItem = pdfData;
        [printController presentAnimated:YES completionHandler:completionHandler];
    }
}


@end
