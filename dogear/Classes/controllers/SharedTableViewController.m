//
//  SharedTableViewController.m
//  dogear
//
//  Created by Joy Tao on 12/7/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "SharedTableViewController.h"

@interface SharedTableViewController ()

@end

@implementation SharedTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIImage * bgImg ;
    CGRect windowBound = [[UIScreen mainScreen] bounds];
    if (windowBound.size.height >= 568.0f)
        bgImg = [UIImage imageNamed:@"dogear-bg-master-blue-568h"];
    else
        bgImg = [UIImage imageNamed:@"dogear-bg-master-blue"];
    self.tableView.backgroundView = [[UIImageView alloc]initWithImage:bgImg];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
