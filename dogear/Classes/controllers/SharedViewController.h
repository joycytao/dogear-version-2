//
//  CameraViewController.h
//  dogear
//
//  Created by Joy Tao on 12/3/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SharedViewController : UIViewController
@property (nonatomic , retain) UIImageView * fullScreenImg;
@property (nonatomic) BOOL isTutorial;
@end
