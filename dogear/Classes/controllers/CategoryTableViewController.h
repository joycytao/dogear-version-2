//
//  CategoryTableViewController.h
//  dogear
//
//  Created by Joy Tao on 12/4/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryTableViewController : SharedTableViewController

- (id)initWithStyle:(UITableViewStyle)style andMode:(EditingMode)mode;
@property (nonatomic , strong) DogEar * thisDogear;

@end
