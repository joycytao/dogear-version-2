//
//  DogEarPhotoViewController.h
//  dogear
//
//  Created by Joy Tao on 12/7/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharedViewController.h"

@interface DogEarPhotoViewController : SharedViewController
@property (nonatomic , strong) DogEar * thisDogear;
@property (nonatomic , strong) NSData * imageData ;

@end
