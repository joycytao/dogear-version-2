//
//  FlaggedTableViewController.h
//  dogear
//
//  Created by Joy Tao on 12/7/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlaggedTableViewController : SharedTableViewController

@property (nonatomic , strong) DogEar * thisDogear;

- (id)initWithStyle:(UITableViewStyle)style andMode:(EditingMode)mode;


@end
