//
//  CameraViewController.m
//  dogear
//
//  Created by Joy Tao on 12/3/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "SharedViewController.h"
#import "DogEarPhotoViewController.h"

@interface SharedViewController () <UINavigationControllerDelegate , UIImagePickerControllerDelegate , UIActionSheetDelegate>
@end

@implementation SharedViewController




#pragma mark - LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGRect windowBound = [[UIScreen mainScreen] bounds];
    
    self.fullScreenImg = [[UIImageView alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    self.fullScreenImg.backgroundColor = [UIColor blackColor];
    self.fullScreenImg.contentMode = (windowBound.size.height >= 568.f)? UIViewContentModeScaleAspectFit : UIViewContentModeTop;
    [self.view addSubview:self.fullScreenImg];
    
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"HasLaunchedOnce"] == NO || self.isTutorial == YES)
    {
        UIImage * bgImg ;
        CGRect windowBound = [[UIScreen mainScreen] bounds];
        if (windowBound.size.height >= 568.0f)
            bgImg = [UIImage imageNamed:@"dogear-bg-instructions-fix-568h"];
        else
            bgImg = [UIImage imageNamed:@"dogear-bg-instructions-fix"];
        self.fullScreenImg.image = bgImg;
        self.fullScreenImg.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        self.fullScreenImg.image = nil;
        self.fullScreenImg.backgroundColor = [UIColor blackColor];

    }
}
- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"HasLaunchedOnce"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - UIImagePickerController Delegate

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        DogEarPhotoViewController * vc = [[DogEarPhotoViewController alloc]init];
        [vc setImageData:UIImagePNGRepresentation(chosenImage)];
        [self.navigationController pushViewController:vc animated:NO];
    }];
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    kAppDelegate.rootViewController.selectedIndex = 0;
}

@end
