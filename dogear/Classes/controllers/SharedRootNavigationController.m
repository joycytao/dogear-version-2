//
//  SharedRootNavigationController.m
//  dogear
//
//  Created by Joy Tao on 12/3/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "SharedRootNavigationController.h"

@interface SharedRootNavigationController ()
@property (nonatomic , retain) UIImageView * overlayView;
@end

@implementation SharedRootNavigationController

#pragma mark- Private
- (void) removeSplash
{
    sleep(3.0f);
    [self.overlayView removeFromSuperview];
}

#pragma mark-
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationBarHidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
