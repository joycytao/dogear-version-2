//
//  DogEarsListViewController.h
//  dogear
//
//  Created by Joy Tao on 12/7/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DogEarsListViewController : SharedTableViewController
@property (nonatomic , strong) NSArray * dogears;

- (id)initWithStyle:(UITableViewStyle)style andMode:(ItemsViewingMode)mode;

@end
