//
//  FlaggedTableViewController.m
//  dogear
//
//  Created by Joy Tao on 12/7/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "FlaggedTableViewController.h"
#import "DogEarsListViewController.h"
@interface FlaggedTableViewController ()
@property (nonatomic) EditingMode thisMode;
@end

@implementation FlaggedTableViewController



- (id)initWithStyle:(UITableViewStyle)style andMode:(EditingMode)mode
{
    self = [super initWithStyle:style];
    if (self) {
        self.thisMode = mode;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Flagged";

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[[SharedHelper sharedHelper]flaggedTitles]count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.textLabel.text = [[[SharedHelper sharedHelper]flaggedTitles] objectAtIndex:indexPath.row];
    
    if ([self.thisDogear.flagged integerValue] != 0 )
    {
        NSInteger checkedRow = [self.thisDogear.flagged integerValue] - 1;
        cell.accessoryType = (indexPath.row == checkedRow)? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    }
    
    
    return cell;
}

#pragma mark - UITableView Delegate 

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.thisMode == kModeNone)
    {
        DogEarsListViewController * vc = [[DogEarsListViewController alloc]initWithStyle:UITableViewStylePlain andMode:kModeByFlagged];
        NSArray * array = [[SharedObjectManager sharedManager]getObjectsByEntityName:@"DogEar" andPredict:[NSPredicate predicateWithFormat:@"flagged == %@", [NSNumber numberWithInteger:indexPath.row + 1]] andSortDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"updated" ascending:NO], nil]];
        [vc setDogears:array];
        [vc.navigationItem setTitle:[[[SharedHelper sharedHelper]flaggedTitles] objectAtIndex:indexPath.row]];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        UITableViewCell * selectedCell = [tableView cellForRowAtIndexPath:indexPath];
        if ([self.thisDogear.flagged integerValue] != 0 )
        {
            NSInteger checkedRow = [self.thisDogear.flagged integerValue] - 1;
            UITableViewCell * checkedCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:checkedRow inSection:0]];
            if (indexPath.row != checkedRow)
            {
                checkedCell.accessoryType = UITableViewCellAccessoryNone;
                selectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
                self.thisDogear.flagged = [NSNumber numberWithInteger:indexPath.row + 1];
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                checkedCell.accessoryType = UITableViewCellAccessoryNone;
                self.thisDogear.category = nil;
            }
        }
        else
        {
            selectedCell.accessoryType = UITableViewCellAccessoryCheckmark;
            self.thisDogear.flagged = [NSNumber numberWithInteger:indexPath.row + 1];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

@end
