//
//  NSString+Additions.h
//  dogear
//
//  Created by Joy Tao on 12/4/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)
+ (NSString*) shortDateAndTimeStyleStringFromDate:(NSDate*)date;

@end
