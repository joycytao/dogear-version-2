//
//  NSString+Additions.m
//  dogear
//
//  Created by Joy Tao on 12/4/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import "NSString+Additions.h"

@implementation NSString (Additions)
+ (NSString*) shortDateAndTimeStyleStringFromDate:(NSDate*)date

{
    NSDateFormatter * df = [[NSDateFormatter alloc]init];
    [df setDateStyle:NSDateFormatterShortStyle];
    [df setTimeStyle:NSDateFormatterShortStyle];
    [df setTimeZone:[NSTimeZone systemTimeZone]];
    
    
    NSString * string = [df stringFromDate:date];
    return string;
}
@end
