//
//  AppDelegate.h
//  dogear
//
//  Created by Joy Tao on 12/3/13.
//  Copyright (c) 2013 Joy Tao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharedRootViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) SharedRootViewController * rootViewController;

@end
